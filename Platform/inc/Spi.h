/*---------------------------------------------------------------------------------------------------------------------

 (c) Copyright 2016 by Ao-to Corporation. All rights reserved.

 The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
 No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
 the specific written permission signed by a legal representative of the Ao-to Corporation.

 Change log:

 ----------     -----------     -------------     ---------------------------------------------------------------------
 Date           Username        Ticket            Change description
 ----------     -----------     -------------     ---------------------------------------------------------------------
 29.10.2016     nicu            AOTO-5            Initial version of the file
 02.11.2016     razvan          AOTO-8            Various updates to have the hw board validation app in place
---------------------------------------------------------------------------------------------------------------------*/

#ifndef _SPI_H
#define _SPI_H

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: FILES INCLUSION
---------------------------------------------------------------------------------------------------------------------*/
#include "Platform_Types.h"

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC DEFINES
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC DATA TYPE DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/
#define SPI_1       0
#define SPI_2       1


typedef enum
{
    Spi_Success_c = 0,
    Spi_Timeout_c
}spiStatus_t;

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC CONSTANTS
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC VARIABLES
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC FUNCTIONS DECLARATIONS
---------------------------------------------------------------------------------------------------------------------*/
void Spi_Init(uint8_t idxSpi);
void Spi_SetCsHigh(uint8_t idxSpi);
void Spi_SetCsLow(uint8_t idxSpi);
spiStatus_t Spi_WriteByte(uint8_t idxSpi, uint8_t addr, uint8_t data);
spiStatus_t Spi_ReadByte(uint8_t idxSpi, uint8_t addr, uint8_t* data);


#endif /* _SPI_H */


