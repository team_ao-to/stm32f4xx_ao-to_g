/*---------------------------------------------------------------------------------------------------------------------

 (c) Copyright 2016 by Ao-to Corporation. All rights reserved.

 The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
 No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
 the specific written permission signed by a legal representative of the Ao-to Corporation.

 Change log:

 ----------     -----------     -------------     ---------------------------------------------------------------------
 Date           Username        Ticket            Change description
 ----------     -----------     -------------     ---------------------------------------------------------------------
 29.10.2016     nicu            AOTO-5            Initial version of the file
 02.11.2016     razvan          AOTO-8            Various updates to have the hw board validation app in place
---------------------------------------------------------------------------------------------------------------------*/

#ifndef _ACC_H
#define _ACC_H

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: FILES INCLUSION
---------------------------------------------------------------------------------------------------------------------*/
#include "Platform_Types.h"
#include "Board.h"

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC DEFINES
---------------------------------------------------------------------------------------------------------------------*/
#define ACC_UNITS_IN_G          0.00006 /* (0.06 mG) */

#define ACC_NUM_AXES            3

#define ACC_AXIS_GRAVITY        2
#define ACC_AXIS_FRONT_REAR     0
#define ACC_AXIS_LEFT_RIGHT     1

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC DATA TYPE DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/
typedef enum
{
    Acc_G_c = 0,
    Acc_G_Plus_c
} acc_t;

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC CONSTANTS
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC VARIABLES
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC FUNCTIONS DECLARATIONS
---------------------------------------------------------------------------------------------------------------------*/
void    Acc_Init(uint8_t idxAcc);
bool_t  Acc_Detect(uint8_t idxAcc);
void    Acc_ReadData(uint8_t idxAcc, int16_t* pData);

#endif /* _ACC_H */
