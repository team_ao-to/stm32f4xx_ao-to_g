/*---------------------------------------------------------------------------------------------------------------------

 (c) Copyright 2016 by Ao-to Corporation. All rights reserved.

 The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
 No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
 the specific written permission signed by a legal representative of the Ao-to Corporation.

 Change log:

 ----------     -----------     -------------     ---------------------------------------------------------------------
 Date           Username        Ticket            Change description
 ----------     -----------     -------------     ---------------------------------------------------------------------
 20.09.2016     nicu            N/A               Initial version of the file
---------------------------------------------------------------------------------------------------------------------*/

#ifndef _NVM_H
#define _NVM_H

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: FILES INCLUSION
---------------------------------------------------------------------------------------------------------------------*/
#include "Platform_Types.h"

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC DEFINES
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC DATA TYPE DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/
typedef enum
{
    Nvm_Success_c = 0,
    Nvm_Error_c,        /* Generic error. */
    Nvm_Initialized_c,  /* Found nothing in flash, using default values and initialzed the flash*/
    Nvm_EraseError_c,
    Nvm_WriteError_c,
    Nvm_ReadError_c
}nvmStatus_t;

/*  NVM data block descriptor: pointer to allocated memory to hold the data, and lenght.
 */
typedef struct
{
    void*       pData;   /* Pointer to the data structure to be saved to NVM */
    uint32_t    dataId;    /* Unique ID for each new data structure pointed by data_ptr */
    uint16_t    lenght;     /* Lenght of the data structure */
}nvmDescriptor_t;

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC CONSTANTS
---------------------------------------------------------------------------------------------------------------------*/
/* Variable to be defined by the application. */
extern const nvmDescriptor_t nvmDescriptor;

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC VARIABLES
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC FUNCTIONS DECLARATIONS
---------------------------------------------------------------------------------------------------------------------*/
nvmStatus_t    Nvm_Init();
nvmStatus_t    Nvm_Save();
nvmStatus_t    Nvm_Restore();

#endif /* _NVM_H */
