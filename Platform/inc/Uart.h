/*---------------------------------------------------------------------------------------------------------------------

 (c) Copyright 2016 by Ao-to Corporation. All rights reserved.

 The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
 No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
 the specific written permission signed by a legal representative of the Ao-to Corporation.

 Change log:

 ----------     -----------     -------------     ---------------------------------------------------------------------
 Date           Username        Ticket            Change description
 ----------     -----------     -------------     ---------------------------------------------------------------------
  25.10.2016     nicu            N/A              Initial version of the file
  01.11.2016     nicu            AOTO-6           Added support for multiple UART instances
  02.11.2016     razvan          AOTO-8           Various updates to have the hw board validation app in place
  05.11.2016     nicu            AOTO-9           Added read functionality
--------------------------------------------------------------------------------------------------------------------*/

#ifndef _UART_H
#define _UART_H

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: FILES INCLUSION
---------------------------------------------------------------------------------------------------------------------*/
#include "Platform_Types.h"

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC DEFINES
---------------------------------------------------------------------------------------------------------------------*/
#define UART_RECEIVE_BUFFER_SIZE    64
/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC DATA TYPE DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/
typedef enum
{
    Uart_Success_c = 0,
    Uart_Error_c,        /* Generic error. */
    Uart_SendError_c,
    Uart_Overrun_c,     /* Receive overrun*/
    Uart_ReadError_c
}uartStatus_t;

typedef enum
{
    Uart_SerialComm_c = 0,
    Uart_Bluetooth_c
}uart_t;

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC CONSTANTS
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC VARIABLES
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC FUNCTIONS DECLARATIONS
---------------------------------------------------------------------------------------------------------------------*/
void Uart_Init(uint8_t idxUart, uint32_t baudrate, uint16_t treshold, void (*pfCallback)(uint8_t));
uartStatus_t Uart_Send(uint8_t idxUart, uint8_t * pBuffer, uint16_t length);
uartStatus_t Uart_Read(uint8_t idxUart, uint8_t *pBuffer, uint16_t maxLength, uint16_t* pRead);

#endif /* _UART_H */
