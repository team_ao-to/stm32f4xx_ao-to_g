/*---------------------------------------------------------------------------------------------------------------------

 (c) Copyright 2016 by Ao-to Corporation. All rights reserved.

 The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets. 
 No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without 
 the specific written permission signed by a legal representative of the Ao-to Corporation.

 Change log:

 ----------     -----------     -------------     ---------------------------------------------------------------------
 Date           Username        Ticket            Change description
 ----------     -----------     -------------     ---------------------------------------------------------------------
 22.10.2016     razvan          AOTO-1            Add Led component in the platform folder
---------------------------------------------------------------------------------------------------------------------*/

#ifndef _LED_H
#define _LED_H

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: FILES INCLUSION
---------------------------------------------------------------------------------------------------------------------*/
#include "Platform_Types.h"

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC DEFINES
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC DATA TYPE DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/
typedef enum 
{
    Led_Status_c = 0,
    Led_G_Plus_c,
    Led_Uart_c
} led_t;

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC CONSTANTS
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC VARIABLES
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC FUNCTIONS DECLARATIONS
---------------------------------------------------------------------------------------------------------------------*/
void Led_Init(void);
void Led_On(led_t led, bool_t isOn);


#endif /* _LED_H */
