/*---------------------------------------------------------------------------------------------------------------------

 (c) Copyright 2016 by Ao-to Corporation. All rights reserved.

 The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
 No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
 the specific written permission signed by a legal representative of the Ao-to Corporation.

 Change log:

 ----------     -----------     -------------     ---------------------------------------------------------------------
 Date           Username        Ticket            Change description
 ----------     -----------     -------------     ---------------------------------------------------------------------
 20.09.2016     nicu            N/A 	          Initial version of the file
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: FILES INCLUSION
---------------------------------------------------------------------------------------------------------------------*/
#include "nvm.h"
#include <string.h>         /* Oups...or we define our own. I'd go with the tools stdlib.*/
#include <stddef.h>
#include "stm32f4xx_flash.h"

/*
 *  NVM stategy for writing multiple times in the same sector.
 *      The data structure that will be saved in the NVM is unknown by the NVM module, only the size
 *  is known, plus an ID that needs to be unique per structure (e.g. structure might change, but
 *  the size is the same -> thus unless was have an ID, we might load the wrong data).
 *      Each sector will have a header (see structure below), and after the header we will have
 *  a variable array of bit pairs (slot bitmap). Each pair of bits represent a data slot that can be used on the
 *  sector. A variable number of slots (used or unused) are found after the header, and after the
 * variable array of bit pairs.
 *      A bit pair can be in the following states:
 *          - "11" -> the data slot is unused. When writing data, the first "11" gives us the first unused slot.
 *          - "01" -> the data slot is unreliable. A write to the slot was attempted, but was not finalized.
 *          - "00" -> the data slot is used. The last "00" bit pair in the array gives us the slot to load.
 *      The process for reading the last saved data slot (on the active sector, with a validated header):
 *          - we parse the slot bitmap (array of bit pairs), left to right.
 *          - the position in the slot bitmap of the first "00" bit pair will indicate the active data slot.
 *      The process of writing a new data slot (executing a new save):
 *          - the first free data slot is identified in the slot bitmap = the first "11" bit pair
 *          - the bit pair is changed to "01", indicating that a new write is attempted
 *          - the data slot is written
 *          - the bit pair is changed to "00", indicating that a write was successful
 *      The header size will be a multiple of 4.
 *      The slot bitmap size will be a multiple of 4.
 *      The data slot size will be a multiple of 4.
 *
 */

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE DEFINES
---------------------------------------------------------------------------------------------------------------------*/
#define SIGNATURE       0xFECAEFBE      /* Signature for an initialized FLASH sector. */
#define USED_SECTORS    2               /* Number of used gSectors. */
#define SECTOR_SIZE     16*1024

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE DATA TYPE DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/
/*  Header for the sector. */
typedef struct
{
    uint32_t    signature;  /* Tells us if it is formatted as a used NVM sector or not. */
    uint8_t     counter;    /* Highest counter determines active sector. 0xFF is not valid. Takes care of wrap-around.*/
    uint32_t    dataId;    /* Copy of the application data structure ID */
    uint32_t    dataLength;    /* Application data structure size.*/
    uint32_t    dataSlots; /* How many data slots I can save on the sector, after overhead and alignment*/
    uint16_t    offset;     /* From the beginning of the sector, the offset to the first slot */
}sectorHeader_t;

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE CONSTANTS
---------------------------------------------------------------------------------------------------------------------*/
/*  Start adresses for the two gSectors we will use. As sector 0 seems to be needed for the IRQs,
 * we will use sector 1 and sector 2 of the FLASH.
 */
static const uint16_t gSectors[USED_SECTORS] = {FLASH_Sector_1, FLASH_Sector_2};
static const uint32_t gSectorAddr[USED_SECTORS] = {0x08004000, 0x08008000};

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE VARIABLES
---------------------------------------------------------------------------------------------------------------------*/
/*  Current sector index in the gSectors[] array of start adresses. */
static uint8_t   gCurrentSector;
/*  Current counter for the active sector */
static uint8_t   gCurrentCounter;
/*  Current active data slot - the one we will read from. Writing will use the next one. */
static int32_t gCurrentSlot;

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC CONSTANTS
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC VARIABLES
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE FUNCTIONS DECLARATIONS
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE FUNCTIONS DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/
/*
 * Compute the number of slots that we need given the size of the data set, and the offset to the slots.
 * The resut is copied in a header structure that is provided by the caller.
 */
static nvmStatus_t ComputeSlots(sectorHeader_t* pHeader)
{
    int32_t bitmapSize;

    /* Minimalistic checks*/
    if(pHeader->dataLength == 0)
    {
        return Nvm_Error_c;
    }

    /* Calculate the number of bytes needed in the slot bitmap, taking into account the fact that we will need space
     *  after the header for the slot bitmap itself, before the data slots.
     */
    bitmapSize = (int32_t)((SECTOR_SIZE - sizeof(sectorHeader_t)))/(1+4*(pHeader->dataLength));
    /* Make sure that we have enough slots as the converson above truncates to the lowest integer.*/
    bitmapSize++;
    /* Make sure it is a multiple of 4 */
    if((bitmapSize%4) != 0)
    {
        bitmapSize = ((bitmapSize >> 2) << 2) + 4;
    }
    /* Calculate how many data slots we can save in the remaining space in the sector */
    pHeader->dataSlots = (int32_t)(SECTOR_SIZE - sizeof(sectorHeader_t) - bitmapSize)/pHeader->dataLength;
    /* The rest */
    pHeader->offset = sizeof(sectorHeader_t) + bitmapSize;

    return Nvm_Success_c;
}

/* Erase the entire sector then create the header. The counter for the sector is set as 0xFF, marking it as inactive.
 * After the data set is written in the sector, we will have to write the counter also, making the sector as active.
 */
static nvmStatus_t FormatSector(uint8_t sector)
{
    FLASH_Status  flashStatus;
    sectorHeader_t header = {.signature = SIGNATURE, .counter = 0xFF, .dataId = nvmDescriptor.dataId, .dataLength = nvmDescriptor.lenght, 0, 0};
    int32_t i;

    if(ComputeSlots(&header) != Nvm_Success_c)
    {
        return Nvm_Error_c;
    }

    FLASH_Unlock();
    flashStatus = FLASH_EraseSector(gSectors[sector], VoltageRange_3);
    if(flashStatus != FLASH_COMPLETE)
    {
        FLASH_Lock();
        return Nvm_EraseError_c;
    }

    /* Write the header.*/
    for(i = 0; i < sizeof(sectorHeader_t); i++)
    {
        flashStatus = FLASH_ProgramByte(gSectorAddr[sector] + i, *((char*)(&header) + i));
        if(flashStatus != FLASH_COMPLETE)
        {
            FLASH_Lock();
            return Nvm_WriteError_c;
        }
    }

    FLASH_Lock();
    return Nvm_Success_c;

}

/*
 * Write the counter to the specified sector, making the sector active.
 */
static nvmStatus_t WriteCounter(uint8_t sector, uint8_t counter)
{
    FLASH_Status  flashStatus;

    FLASH_Unlock();
    /* Write the counter.*/
    flashStatus = FLASH_ProgramByte(gSectorAddr[sector] + offsetof(sectorHeader_t, counter), counter);
    if(flashStatus != FLASH_COMPLETE)
    {
        FLASH_Lock();
        return Nvm_WriteError_c;
    }

    FLASH_Lock();
    return Nvm_Success_c;

}


/*
 * Will check to see if the flash is initialized, with the correct data set.
 */
static bool_t IsFlashInitialized()
{
    sectorHeader_t* pSh0;
    sectorHeader_t* pSh1;

    /* Init the pointers the sector headers and determine which one is active.*/
    pSh0 = (sectorHeader_t*)gSectorAddr[0];
    pSh1 = (sectorHeader_t*)gSectorAddr[1];

    if((pSh0->signature == SIGNATURE)&&(pSh0->counter != 0xFF)&&(pSh0->dataId == nvmDescriptor.dataId))
    {
        if((pSh1->signature == SIGNATURE)&&(pSh1->counter != 0xFF)&&(pSh1->dataId == nvmDescriptor.dataId))
        {
            if((pSh1->counter > pSh0->counter) && ((pSh1->counter - pSh0->counter) == 1))
            {
                gCurrentSector = 1;
                gCurrentCounter = pSh1->counter;
                return TRUE;
            }
            else
            {
                gCurrentSector = 0;
                gCurrentCounter = pSh0->counter;
                return TRUE;
            }
        }
        else
        {
            gCurrentSector = 0;
            gCurrentCounter = pSh0->counter;
            return TRUE;
        }
    }
    else if((pSh1->signature == SIGNATURE)&&(pSh1->counter != 0xFF)&&(pSh1->dataId == nvmDescriptor.dataId))
    {
        gCurrentSector = 1;
        gCurrentCounter = pSh1->counter;
        return TRUE;
    }

    /* The data id does not match, we will need to re-initialize the flash. */
    return FALSE;
}

/*
 * Determine the current target slot to read in the provided sector. The sector header needs to be initialized.
 */
static int32_t GetReadSlot(sectorHeader_t* pHeader)
{
    uint8_t* pSlotBitmap = (uint8_t*)pHeader + sizeof(sectorHeader_t);
    int32_t bitmapSize = pHeader->offset - sizeof(sectorHeader_t);
    int32_t i;
    /* Start parsing the slot bitmap, from the end of the bitmap, looking for the first "00" */
    for(i = (bitmapSize - 1); i >= 0; i--)
    {
        if((pSlotBitmap[i])!= 0xFF)
        {
            if((pSlotBitmap[i] & 0x03) == 0x00) return (i*4 + 3);
            if((pSlotBitmap[i] & 0x0C) == 0x00) return (i*4 + 2);
            if((pSlotBitmap[i] & 0x30) == 0x00) return (i*4 + 1);
            if((pSlotBitmap[i] & 0xC0) == 0x00) return (i*4);
        }
    }
    /* if we got here, there was no active slot */
    return -1;
}

/*
 * Determine the current target slot to write in the provided sector. The sector header needs to be initialized.
 */
static int32_t GetWriteSlot(sectorHeader_t* pHeader)
{
    uint8_t* pSlotBitmap = (uint8_t*)pHeader + sizeof(sectorHeader_t);
    int32_t bitmapSize = pHeader->offset - sizeof(sectorHeader_t);
    int32_t slot;
    int32_t i;
    /* Start parsing the slot bitmap, from the start of the bitmap, looking for the first "11".
     * We need to check that the returned slot number is smaller than the maximum that can be stored in the sector.
     */
    for(i = 0; i <= bitmapSize; i++)
    {
        if((pSlotBitmap[i])!= 0x00)
        {
            if((pSlotBitmap[i] & 0xC0) == 0xC0)
            {
                slot = i*4;
                break;
            }
            if((pSlotBitmap[i] & 0x30) == 0x30)
            {
                slot = i*4 + 1;
                break;
            }
            if((pSlotBitmap[i] & 0x0C) == 0x0C)
            {
                slot = i*4 + 2;
                break;
            }
            if((pSlotBitmap[i] & 0x03) == 0x03)
            {
                slot = i*4 + 3;
                break;
            }
        }
    }
    return ( (slot < pHeader->dataSlots) ? slot : -1);
}

/*
 * Write data in the indicated sector and data slot.
 */
static nvmStatus_t WriteSlot(sectorHeader_t* pHeader, int32_t slot)
{
    FLASH_Status  flashStatus;
    int32_t slotOffset;   /* Start adress for the slot.*/
    uint8_t* pSlotBitmap; /* Byte in the slot bitmap where we have the marker for the actual slot.*/
    uint8_t temp, mask;
    int32_t i;

    slotOffset = pHeader->offset + slot*nvmDescriptor.lenght;
    pSlotBitmap = (uint8_t*)pHeader + sizeof(sectorHeader_t) + (int32_t)slot/4;


    FLASH_Unlock();
    /* Mark the beginning of the writing process for the indicated data slot in the slot bitmap. */
    mask = ~(0x80 >> (2*(slot&0x00000003)));
    temp = (*pSlotBitmap) & mask;
    flashStatus = FLASH_ProgramByte((uint32_t)pSlotBitmap, temp);

    /* Write the counter.*/
    for(i = 0; i < nvmDescriptor.lenght; i++)
    {
        flashStatus = FLASH_ProgramByte((uint32_t)pHeader + slotOffset + i, *((char*)nvmDescriptor.pData + i));
        if(flashStatus != FLASH_COMPLETE)
        {
            FLASH_Lock();
            return Nvm_WriteError_c;
        }
    }
    /* Mark the end of the writing process for the slot in the slto bitmap. */
    mask = (mask >> 1) | 0x80;
    temp = (*pSlotBitmap) & mask;
    flashStatus = FLASH_ProgramByte((uint32_t)pSlotBitmap, temp);
    /* All data was written OK if we got here. */
    FLASH_Lock();
    return Nvm_Success_c;

}

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC FUNCTIONS DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/
/*
 *  Look for the Flash sector that holds the valid data set, read it, initialize the pointers
 * alowing other modules to access it.
 */
nvmStatus_t    Nvm_Init()
{
    nvmStatus_t  nvmStat = Nvm_Success_c; /* Assume success */
    if(!IsFlashInitialized())
    {
        /* The flash is not initialized. Start with sector 0, format it, then write the counter to mark it as active.*/
        gCurrentSector = 0;
        gCurrentCounter = 0;
        nvmStat = FormatSector(gCurrentSector);
        if(nvmStat == Nvm_Success_c)
        {
            nvmStat = WriteCounter(gCurrentSector, gCurrentCounter);
        }
        if(nvmStat != Nvm_Success_c)
        {
            return nvmStat;    /* Somethign failed, return the error code to the caller. */
        }
        else
        {
            nvmStat = Nvm_Initialized_c;   /* The flash is formatted but it contains no values.*/
        }
    }
    else
    {
        nvmStat = Nvm_Restore();
    }

    return nvmStat;
}

/*
 *  Save the current data set into flash. The flash needs to be initialized.
 */
nvmStatus_t    Nvm_Save(void )
{
    nvmStatus_t  nvmStat;
    uint8_t target_sector;
    uint8_t target_counter;
    int32_t target_slot;

    /* Determine the target slot*/
    target_slot = GetWriteSlot((sectorHeader_t*)gSectorAddr[gCurrentSector]);
    if(target_slot != -1)
    {
        /* Write the data in the selected data slot */
        nvmStat = WriteSlot((sectorHeader_t*)gSectorAddr[gCurrentSector], target_slot);
    }
    else    /* we need to change the sector */
    {
        target_sector = (gCurrentSector + 1)%USED_SECTORS;
        target_counter = (gCurrentCounter + 1)%0xFF;
        target_slot = 0;

        nvmStat = FormatSector(target_sector);

        if(nvmStat == Nvm_Success_c)
        {
            /* Write the data in the first data slot */
            nvmStat = WriteSlot((sectorHeader_t*)gSectorAddr[target_sector], target_slot);
            if(nvmStat == Nvm_Success_c)
            {
                /* Activate the sector*/
                nvmStat = WriteCounter(target_sector, target_counter);
                if(nvmStat == Nvm_Success_c)
                {
                    /* All good, we saved the data, update the active flash sector and current counter. */
                    gCurrentSector = target_sector;
                    gCurrentCounter = target_counter;
                }
            }
        }
        else
        {
            return nvmStat;    /* Somethign failed, return the error code to the caller. */
        }

    }

    gCurrentSlot = target_slot;
    return nvmStat;
}

/*
 *  Reload the data currently saved in the active flash sector. Will revrite the current RAM values.
 *  The flash needs to be initialized before calling this function.
 */
nvmStatus_t    Nvm_Restore(void)
{
    /* Assume that the flash is formatted. Determine the slot and try to read the data. */
    gCurrentSlot = GetReadSlot((sectorHeader_t*)gSectorAddr[gCurrentSector]);
    if(gCurrentSlot == -1)
    {
        /* The sector is formatted but no data was saved, nothig to read.*/
        return Nvm_Initialized_c;
    }
    else
    {
        /* Active data slot identified. Read the data.*/
        memcpy((void*)nvmDescriptor.pData, (void*)(gSectorAddr[gCurrentSector] + ((sectorHeader_t*)gSectorAddr[gCurrentSector])->offset + gCurrentSlot*nvmDescriptor.lenght), nvmDescriptor.lenght);
    }
    return Nvm_Success_c;
}