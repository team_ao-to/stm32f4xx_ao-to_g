/*---------------------------------------------------------------------------------------------------------------------

 (c) Copyright 2016 by Ao-to Corporation. All rights reserved.

 The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
 No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
 the specific written permission signed by a legal representative of the Ao-to Corporation.

 Change log:

 ----------     -----------     -------------     ---------------------------------------------------------------------
 Date           Username        Ticket            Change description
 ----------     -----------     -------------     ---------------------------------------------------------------------
 29.10.2016     nicu            AOTO-5            Initial version of the file
 02.11.2016     razvan          AOTO-8            Various updates to have the hw board validation app in place
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: FILES INCLUSION
---------------------------------------------------------------------------------------------------------------------*/
#include "Spi.h"
#include "stm32f4xx.h"
#include "stm32f4xx_spi.h"
#include "stm32f4xx_gpio.h"
#include "Board.h"

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE DEFINES
---------------------------------------------------------------------------------------------------------------------*/
/* Number of GPIO pins used by every SPI interface */
#define NUM_PINS_PER_SPI    4

/* Timeout value for SPI read/write operations */
#define SPI_OP_TIMEOUT      100000

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE DATA TYPE DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/
/* Enum defining the SPI pin names */
typedef enum
{
    SPI_MISO = 0,
    SPI_MOSI,
    SPI_SCK,
    SPI_CS
}spiPinName_t;

/* Structure containing information about a SPI pin */
typedef struct
{
    uint16_t        spiPin;         /* Pin number           */
    GPIO_TypeDef*   spiPinPort;     /* Pin port             */
    uint32_t        spiPinPortClk;  /* Pin port clock       */
    uint8_t         spiPinSource;   /* Pin source           */
    uint8_t         spiPinAF;       /* Alternate function   */
}spiPinConfig_t;

/* Structure containing information about a SPI unit */
typedef struct
{
   SPI_TypeDef*     spiUnit;            /* SPI Unit                  */
   __IO uint32_t*   pSpiClkEnableReg;   /* SPI Clock Enable Register */
   uint32_t         spiClkEnableMask;   /* SPI Clock Enable Mask     */
}gSpiConfig_t;

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE CONSTANTS
---------------------------------------------------------------------------------------------------------------------*/
/* Structure containing SPI units configuration information */
static const gSpiConfig_t gSpiConfig[SPI_NUM] =
{
    SPI_CONFIG
};

/* Structure containing SPI pins configuration information */
static const spiPinConfig_t gSpiPinsConfig[SPI_NUM][NUM_PINS_PER_SPI] =
{
    SPI_PINS_CONFIG
};

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE VARIABLES
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC CONSTANTS
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC VARIABLES
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE FUNCTIONS DECLARATIONS
---------------------------------------------------------------------------------------------------------------------*/
static void InitSpiGpioPins(uint8_t idxSpi);
static inline spiStatus_t WaitSpiOpCompletion(uint8_t idxSpi);

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE FUNCTIONS DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/
static void InitSpiGpioPins(uint8_t idxSpi)
{
    GPIO_InitTypeDef    gpioInitStruct;
    uint8_t             i;
    spiPinConfig_t*     pSpiGpioPinsConfig;

    /* Initialize all GPIO pins that belong to current SPI instance */
    for(i=0; i<NUM_PINS_PER_SPI; i++)
    {
        /* Keep a pointer to spi GPIO pins config structure for ease of use */
        pSpiGpioPinsConfig = (spiPinConfig_t*)&gSpiPinsConfig[idxSpi][i];
        /* Enable clock for GPIO pin port */
        RCC_AHB1PeriphClockCmd(pSpiGpioPinsConfig->spiPinPortClk, ENABLE);
        /* Start filling gpio init structure */
        gpioInitStruct.GPIO_Pin    = pSpiGpioPinsConfig->spiPin;
        gpioInitStruct.GPIO_OType  = GPIO_OType_PP;
        gpioInitStruct.GPIO_Speed  = GPIO_Speed_50MHz;
        /* If pin is SPI_CS, we need to configure it as GPIO. All other pins need to
        be configured with alternate function set to SPI */
        if(i == SPI_CS)
        {
            gpioInitStruct.GPIO_Mode   = GPIO_Mode_OUT;
            gpioInitStruct.GPIO_PuPd   = GPIO_PuPd_UP;
            GPIO_Init(pSpiGpioPinsConfig->spiPinPort, &gpioInitStruct);
            /* Set CS high */
            Spi_SetCsHigh(idxSpi);
        }
        else
        {
            GPIO_PinAFConfig(pSpiGpioPinsConfig->spiPinPort, pSpiGpioPinsConfig->spiPinSource, pSpiGpioPinsConfig->spiPinAF);
            gpioInitStruct.GPIO_Mode    = GPIO_Mode_AF;
            gpioInitStruct.GPIO_PuPd    = GPIO_PuPd_DOWN;
            GPIO_Init(pSpiGpioPinsConfig->spiPinPort, &gpioInitStruct);
        }
    }
}

static inline spiStatus_t WaitSpiOpCompletion(uint8_t idxSpi)
{
    uint32_t timeout = SPI_OP_TIMEOUT;
    while (((gSpiConfig[idxSpi].spiUnit)->SR & (SPI_SR_TXE | SPI_SR_RXNE)) == 0 || (((gSpiConfig[idxSpi].spiUnit)->SR & SPI_SR_BSY)))
    {
        timeout--;
        if (timeout ==0)
            return Spi_Timeout_c;
    }
    return Spi_Success_c;
}

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC FUNCTIONS DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/
void Spi_Init(uint8_t idxSpi)
{
    SPI_InitTypeDef spiInitStruct;

    /* Initialize GPIO pins used by SPI */
    InitSpiGpioPins(idxSpi);
    /* Initialize SPI with default settings */
    SPI_StructInit(&spiInitStruct);
    /* Enable SPI peripheral clock */
    *gSpiConfig[idxSpi].pSpiClkEnableReg |= gSpiConfig[idxSpi].spiClkEnableMask;
    /* Fill SPI settings */
    spiInitStruct.SPI_DataSize             = SPI_DataSize_8b;
    spiInitStruct.SPI_BaudRatePrescaler    = SPI_BaudRatePrescaler_32;
    spiInitStruct.SPI_Direction            = SPI_Direction_2Lines_FullDuplex;
    spiInitStruct.SPI_FirstBit             = SPI_FirstBit_MSB;
    spiInitStruct.SPI_Mode                 = SPI_Mode_Master;
    spiInitStruct.SPI_NSS                  = SPI_NSS_Soft;
    spiInitStruct.SPI_CPOL                 = SPI_CPOL_Low;
    spiInitStruct.SPI_CPHA                 = SPI_CPHA_1Edge;
    /* Disable SPI peripheral before initializing it */
    gSpiConfig[idxSpi].spiUnit->CR1 &= ~SPI_CR1_SPE;
    /* Initialize SPI peripheral */
    SPI_Init(gSpiConfig[idxSpi].spiUnit, &spiInitStruct);
    /* Enable SPI peripheral */
    gSpiConfig[idxSpi].spiUnit->CR1 |= SPI_CR1_SPE;
}

void Spi_SetCsHigh(uint8_t idxSpi)
{
    /* Set the chip select high */
    gSpiPinsConfig[idxSpi][SPI_CS].spiPinPort->BSRRL = gSpiPinsConfig[idxSpi][SPI_CS].spiPin;
}

void Spi_SetCsLow(uint8_t idxSpi)
{
    /* Set the chip select low */
    gSpiPinsConfig[idxSpi][SPI_CS].spiPinPort->BSRRH = gSpiPinsConfig[idxSpi][SPI_CS].spiPin;
}

spiStatus_t Spi_WriteByte(uint8_t idxSpi, uint8_t addr, uint8_t data)
{
    spiStatus_t retVal = Spi_Success_c;
    /* Write out address */
    gSpiConfig[idxSpi].spiUnit->DR = addr;
    /* Wait for completion */
    retVal = WaitSpiOpCompletion(idxSpi);
    /* Fake read of the received data */
    (void)gSpiConfig[idxSpi].spiUnit->DR;

    /* Write out data */
    gSpiConfig[idxSpi].spiUnit->DR = data;
    /* Wait for completion */
    retVal = WaitSpiOpCompletion(idxSpi);
    /* Fake read of the received data */
    (void)gSpiConfig[idxSpi].spiUnit->DR;

    return retVal;
}

spiStatus_t Spi_ReadByte(uint8_t idxSpi, uint8_t addr, uint8_t* data)
{
    spiStatus_t retVal = Spi_Success_c;
    /* Set read bit */
    addr |= 0x80;
    /* Write the address */
    Spi_WriteByte(idxSpi, addr, 0x00);
    /* Wait for completion */
    retVal = WaitSpiOpCompletion(idxSpi);
    /* Copy the received data to the caller */
    *data = gSpiConfig[idxSpi].spiUnit->DR;

    return retVal;
}
