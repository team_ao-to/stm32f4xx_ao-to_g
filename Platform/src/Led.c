/*---------------------------------------------------------------------------------------------------------------------

 (c) Copyright 2016 by Ao-to Corporation. All rights reserved.

 The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets. 
 No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without 
 the specific written permission signed by a legal representative of the Ao-to Corporation.

 Change log:

 ----------     -----------     -------------     ---------------------------------------------------------------------
 Date           Username        Ticket            Change description
 ----------     -----------     -------------     ---------------------------------------------------------------------
 22.10.2016     razvan          AOTO-1            Add Led component in the platform folder
 12.11.2016     razvan          AOTO-11           Update LED info in boards header files following ACC, SPI, UART format
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: FILES INCLUSION
---------------------------------------------------------------------------------------------------------------------*/
#include "Led.h"
#include "stm32f4xx.h"  
#include "Board.h"

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE DEFINES
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE DATA TYPE DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/
/* Structure containing information about a LED */
typedef struct
{
    uint16_t        ledPin;         /* Pin number           */
    GPIO_TypeDef*   ledPinPort;     /* Pin port             */
    uint32_t        ledPinPortClk;  /* Pin port clock       */
}ledPinConfig_t;

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE CONSTANTS
---------------------------------------------------------------------------------------------------------------------*/
/* Structure containing LEDs configuration information */
static const ledPinConfig_t gLedPinsConfig[LED_NUM] =
{
    LED_PINS_CONFIG
};

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE VARIABLES
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC CONSTANTS
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC VARIABLES
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE FUNCTIONS DECLARATIONS
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE FUNCTIONS DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC FUNCTIONS DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/
void Led_Init(void)
{
  GPIO_InitTypeDef  GPIO_InitStructure;
  uint8_t i;
  
  for(i = 0; i < LED_NUM; i++)
  {
    /* Enable the GPIO_LED Clock */
    RCC_AHB1PeriphClockCmd(gLedPinsConfig[i].ledPinPortClk, ENABLE);

    /* Configure the GPIO_LED pin */
    GPIO_InitStructure.GPIO_Pin   = gLedPinsConfig[i].ledPin;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init((GPIO_TypeDef*)gLedPinsConfig[i].ledPinPort, &GPIO_InitStructure);
    
    /* Turn LED off */
    Led_On((led_t)i, FALSE);
  }
}

void Led_On(led_t led, bool_t isOn)
{
    if(isOn)
        ((GPIO_TypeDef*)gLedPinsConfig[led].ledPinPort)->BSRRL = gLedPinsConfig[led].ledPin;
    else
        ((GPIO_TypeDef*)gLedPinsConfig[led].ledPinPort)->BSRRH = gLedPinsConfig[led].ledPin;
}

