/*---------------------------------------------------------------------------------------------------------------------

 (c) Copyright 2016 by Ao-to Corporation. All rights reserved.

 The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
 No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
 the specific written permission signed by a legal representative of the Ao-to Corporation.

 Change log:

 ----------     -----------     -------------     ---------------------------------------------------------------------
 Date           Username        Ticket            Change description
 ----------     -----------     -------------     ---------------------------------------------------------------------
 25.10.2016     nicu            N/A 	          Initial version of the file
 01.11.2016     nicu            AOTO-6 	          Added support for multiple UART instances
 02.11.2016     razvan          AOTO-8            Various updates to have the hw board validation app in place
 05.11.2016     nicu            AOTO-9 	          Added read functionality
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: FILES INCLUSION
---------------------------------------------------------------------------------------------------------------------*/
#include "Uart.h"
#include "stm32f4xx.h"
#include "Board.h"

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE DEFINES
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE DATA TYPE DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/
/* Structure containing information about the USART instance */
typedef struct
{
    USART_TypeDef*  usartInstance;        /* USART unit */
    uint8_t         usartIRQn;            /* USART IRQ  */
   __IO uint32_t*   pUsartClkEnableReg;   /* USART Clock Enable Register */
   uint32_t         usartClkEnableMask;   /* USART Clock Enable Mask     */
}uartUsartInfo_t;

/* Structure containing information about the UART GPIO port and pins */
typedef struct
{
    GPIO_TypeDef*   uartGpioPort;       /* GPIO Port        */
    uint32_t        uartGpioPortClk;    /* GPIO Port clock  */
    uint16_t        uartTxPin;          /* TX pin           */
    uint16_t        uartRxPin;          /* RX pin           */
    uint8_t         uartTxPinSource;    /* TX pin source    */
    uint8_t         uartRxPinSource;    /* RX pin source    */
    uint8_t         uartAF;             /* Alternate func   */
}uartPortInfo_t;

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE CONSTANTS
---------------------------------------------------------------------------------------------------------------------*/
/* Structure containing USART units information */
static const uartUsartInfo_t gUsartInfo[UART_NUM] =
{
    USART_INFO
};

/* Structure containing UART GPIO port and pin configuration information */
static const uartPortInfo_t gPortInfo[UART_NUM] =
{
    UART_PORT_INFO
};
/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE VARIABLES
---------------------------------------------------------------------------------------------------------------------*/
uint8_t     gReceiveBuffer[UART_NUM][UART_RECEIVE_BUFFER_SIZE];
void        (*pfReceiveCallback[UART_NUM])(uint8_t);
uint8_t     gReceiveTreshold[UART_NUM];
uint16_t    gReceiveReadIdx[UART_NUM];
uint16_t    gReceiveWriteIdx[UART_NUM];
bool_t      gReceiveOverflow[UART_NUM];
/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC CONSTANTS
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC VARIABLES
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE FUNCTIONS DECLARATIONS
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE FUNCTIONS DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC FUNCTIONS DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/
void Uart_Init(uint8_t idxUart, uint32_t baudrate, uint16_t treshold, void (*pfCallback)(uint8_t))
{
    GPIO_InitTypeDef GPIO_InitStruct;   /* GPIO pins used as TX and RX */
    USART_InitTypeDef USART_InitStruct; /* USART initilization */
    NVIC_InitTypeDef NVIC_InitStructure;/* NVIC initialization */

    /* Enable APB peripheral clock for the used USART. */
    *gUsartInfo[idxUart].pUsartClkEnableReg |= gUsartInfo[idxUart].usartClkEnableMask;
    /* Enable the peripheral clock for the pins used by the USART */
    RCC_AHB1PeriphClockCmd(gPortInfo[idxUart].uartGpioPortClk, ENABLE);

    /* This sequence sets up the TX and RX pins so they work correctly with the USART peripheral */
    GPIO_InitStruct.GPIO_Pin   = gPortInfo[idxUart].uartTxPin | gPortInfo[idxUart].uartRxPin;
    GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_AF;          /* configure pins as alternate function */
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;      /* IO speed, nothing to do with the baudrate */
    GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;         /* output type is push pull mode (as opposed to open drain) */
    GPIO_InitStruct.GPIO_PuPd  = GPIO_PuPd_UP;          /* activates the pullup resistors on the IO pins */
    GPIO_Init(gPortInfo[idxUart].uartGpioPort, &GPIO_InitStruct);

    /* The RX and TX pins are now connected to their AF so that the selected USART can control the pins */
    GPIO_PinAFConfig(gPortInfo[idxUart].uartGpioPort, gPortInfo[idxUart].uartTxPinSource, gPortInfo[idxUart].uartAF);
    GPIO_PinAFConfig(gPortInfo[idxUart].uartGpioPort, gPortInfo[idxUart].uartRxPinSource, gPortInfo[idxUart].uartAF);

    /* Now the USART_InitStruct is used to define the properties of the used USART */
    USART_InitStruct.USART_BaudRate = baudrate;
    USART_InitStruct.USART_WordLength = USART_WordLength_8b;
    USART_InitStruct.USART_StopBits = USART_StopBits_1;
    USART_InitStruct.USART_Parity = USART_Parity_No;
    USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStruct.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
    USART_Init(gUsartInfo[idxUart].usartInstance, &USART_InitStruct);

    /* Initi receiver params */
    gReceiveTreshold[idxUart] = treshold;
    pfReceiveCallback[idxUart] = pfCallback;
    gReceiveReadIdx[idxUart] = 0;
    gReceiveWriteIdx[idxUart] = 0;
    gReceiveOverflow[idxUart] = FALSE;

    /* Enable receiver interrupts */
    NVIC_InitStructure.NVIC_IRQChannel =  gUsartInfo[idxUart].usartIRQn;// we want to configure the USART interrupts
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 7;// this sets the priority group of the this USART interrupt
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 7;		 // this sets the subpriority inside the group
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			 // the USART interrupts are globally enabled
    NVIC_Init(&NVIC_InitStructure);							 // the properties are passed to the NVIC_Init function which takes care of the low level stuff

    USART_ITConfig(gUsartInfo[idxUart].usartInstance, USART_IT_RXNE, ENABLE); // enable the USART receive interrupt

    /* Enables the complete selected USART peripheral */
    USART_Cmd(gUsartInfo[idxUart].usartInstance, ENABLE);

}

uartStatus_t Uart_Send(uint8_t idxUart, uint8_t * pBuffer, uint16_t length)
{
    uint16_t index = 0;
    while(length)
    {
        /* wait until se can send then send the current character */
        while( !(gUsartInfo[idxUart].usartInstance->SR & 0x00000040) );
        USART_SendData(gUsartInfo[idxUart].usartInstance, pBuffer[index]);
        length--;
        index++;
    }

    return Uart_Success_c;
}

/* If pBuffer is NULL or macLength is 0, this is a peek to see how many bytes we have received*/
uartStatus_t Uart_Read(uint8_t idxUart, uint8_t *pBuffer, uint16_t maxLength, uint16_t* pRead)
{
    uartStatus_t retCode = Uart_Success_c;
    uint16_t bytesAvailable;

    if(gReceiveOverflow[idxUart])
    {
        bytesAvailable = UART_RECEIVE_BUFFER_SIZE;
        retCode = Uart_Overrun_c;
    }
    else if(gReceiveWriteIdx[idxUart] >= gReceiveReadIdx[idxUart])
    {
        bytesAvailable = gReceiveWriteIdx[idxUart] - gReceiveReadIdx[idxUart];
    }
    else
    {
        bytesAvailable = UART_RECEIVE_BUFFER_SIZE - gReceiveReadIdx[idxUart] + gReceiveWriteIdx[idxUart];
    }

    if((pBuffer == NULL)||(maxLength == 0)) /* Just peeking?*/
    {
        *pRead = bytesAvailable;
    }
    else if ((pBuffer != NULL)&&(maxLength > 0)&&(bytesAvailable > 0))
    {
        /* Verify that we will nto try to copy to the user buffer more that there is space for */
        if(bytesAvailable > maxLength)
        {
            bytesAvailable = maxLength;
        }
        /* Copy the bytes, and do not forget to reset the overwrite flag.*/
        while(gReceiveReadIdx[idxUart] != gReceiveWriteIdx[idxUart])
        {
            *(pBuffer++) = gReceiveBuffer[idxUart][gReceiveReadIdx[idxUart]++];
            gReceiveReadIdx[idxUart] = gReceiveReadIdx[idxUart] % UART_RECEIVE_BUFFER_SIZE;
        }
        *pRead = bytesAvailable;
        gReceiveOverflow[idxUart] = FALSE;
    }
    else
    {
        retCode = Uart_Error_c;
    }
    return retCode;
}

/* Single interrupt handler used for the all USART instance. */
void USARTX_IRQHandler(void)
{
    /* Look for the receive interrupt */
    for(uint8_t idxUart = 0; idxUart < UART_NUM; idxUart++)
    {
        if( USART_GetITStatus(gUsartInfo[idxUart].usartInstance, USART_IT_RXNE) )
        {
            uint16_t bytesAvailable;
            /* Reading the DR register clears the RXNE bit (thus the interrupt flag)*/
            gReceiveBuffer[idxUart][gReceiveWriteIdx[idxUart]++] = gUsartInfo[idxUart].usartInstance->DR;
            gReceiveWriteIdx[idxUart] = gReceiveWriteIdx[idxUart] % UART_RECEIVE_BUFFER_SIZE;
            if(gReceiveWriteIdx[idxUart] != gReceiveReadIdx[idxUart])
            {
                if(gReceiveWriteIdx[idxUart] >= gReceiveReadIdx[idxUart])
                {
                    bytesAvailable = gReceiveWriteIdx[idxUart] - gReceiveReadIdx[idxUart];
                }
                else
                {
                    bytesAvailable = UART_RECEIVE_BUFFER_SIZE - gReceiveReadIdx[idxUart] + gReceiveWriteIdx[idxUart];
                }
                if(bytesAvailable == gReceiveTreshold[idxUart])
                {
                    pfReceiveCallback[idxUart](idxUart);
                }
            }
            else
            {
                gReceiveOverflow[idxUart] = TRUE;
            }
            /* Alternate way of clearing the interrupt  - not needed, shown here as example */
            /* USART_ClearITPendingBit(gUsartInfo[idxUart].usartInstance, USART_IT_RXNE); */
        }
    }
}