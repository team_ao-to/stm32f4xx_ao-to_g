/*---------------------------------------------------------------------------------------------------------------------

 (c) Copyright 2016 by Ao-to Corporation. All rights reserved.

 The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
 No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
 the specific written permission signed by a legal representative of the Ao-to Corporation.

 Change log:

 ----------     -----------     -------------     ---------------------------------------------------------------------
 Date           Username        Ticket            Change description
 ----------     -----------     -------------     ---------------------------------------------------------------------
 29.10.2016     nicu            AOTO-5            Initial version of the file
 02.11.2016     razvan          AOTO-8            Various updates to have the hw board validation app in place
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: FILES INCLUSION
---------------------------------------------------------------------------------------------------------------------*/
#include "Acc.h"
#include "Spi.h"
#include "Board.h"

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE DEFINES
---------------------------------------------------------------------------------------------------------------------*/
/* LIS3DSH registers addresses */
#define LIS3DSH_WHO_AM_I_ADDR                0x0F
#define LIS3DSH_CTRL_REG4_ADDR               0x20
#define LIS3DSH_CTRL_REG1_ADDR               0x21
#define LIS3DSH_CTRL_REG2_ADDR               0x22
#define LIS3DSH_CTRL_REG3_ADDR               0x23
#define LIS3DSH_CTRL_REG5_ADDR               0x24
#define LIS3DSH_CTRL_REG6_ADDR               0x25
#define LIS3DSH_STATUS_ADDR                  0x27
#define LIS3DSH_OUT_X_L_ADDR                 0x28
#define LIS3DSH_OUT_X_H_ADDR                 0x29
#define LIS3DSH_OUT_Y_L_ADDR                 0x2A
#define LIS3DSH_OUT_Y_H_ADDR                 0x2B
#define LIS3DSH_OUT_Z_L_ADDR                 0x2C
#define LIS3DSH_OUT_Z_H_ADDR                 0x2D

#define LIS3DSH_DATARATE_POWERDOWN           ((uint8_t)0x00)  /* Power Down Mode*/
#define LIS3DSH_DATARATE_3_125               ((uint8_t)0x10)  /* 3.125 Hz Normal Mode */
#define LIS3DSH_DATARATE_6_25                ((uint8_t)0x20)  /* 6.25  Hz Normal Mode */
#define LIS3DSH_DATARATE_12_5                ((uint8_t)0x30)  /* 12.5  Hz Normal Mode */
#define LIS3DSH_DATARATE_25                  ((uint8_t)0x40)  /* 25    Hz Normal Mode */
#define LIS3DSH_DATARATE_50                  ((uint8_t)0x50)  /* 50    Hz Normal Mode */
#define LIS3DSH_DATARATE_100                 ((uint8_t)0x60)  /* 100   Hz Normal Mode */
#define LIS3DSH_DATARATE_400                 ((uint8_t)0x70)  /* 400   Hz Normal Mode */
#define LIS3DSH_DATARATE_800                 ((uint8_t)0x80)  /* 800   Hz Normal Mode */
#define LIS3DSH_DATARATE_1600                ((uint8_t)0x90)  /* 1600  Hz Normal Mode */

#define LIS3DSH_BLOCK_DATA_UPDATE           ((uint8_t)0x08)

#define LIS3DSH_FULLSCALE_2                  ((uint8_t)0x00)  /* 2 g  */
#define LIS3DSH_FULLSCALE_4                  ((uint8_t)0x08)  /* 4 g  */
#define LIS3DSH_FULLSCALE_6                  ((uint8_t)0x10)  /* 6 g  */
#define LIS3DSH_FULLSCALE_8                  ((uint8_t)0x18)  /* 8 g  */
#define LIS3DSH_FULLSCALE_16                 ((uint8_t)0x20)  /* 16 g */
#define LIS3DSH__FULLSCALE_SELECTION         ((uint8_t)0x38)

#define LIS3DSH_FILTER_BW_800                ((uint8_t)0x00)  /* 800 Hz */
#define LIS3DSH_FILTER_BW_40                 ((uint8_t)0x08)  /* 40 Hz  */
#define LIS3DSH_FILTER_BW_200                ((uint8_t)0x10)  /* 200 Hz */
#define LIS3DSH_FILTER_BW_50                 ((uint8_t)0x18)  /* 50 Hz  */

#define LIS3DSH_SELFTEST_NORMAL              ((uint8_t)0x00)
#define LIS3DSH_SELFTEST_P                   ((uint8_t)0x02)
#define LIS3DSH_SELFTEST_M                   ((uint8_t)0x04)

#define LIS3DSH_X_ENABLE                     ((uint8_t)0x01)
#define LIS3DSH_Y_ENABLE                     ((uint8_t)0x02)
#define LIS3DSH_Z_ENABLE                     ((uint8_t)0x04)
#define LIS3DSH_XYZ_ENABLE                   ((uint8_t)0x07)

#define LIS3DSH_SERIALINTERFACE_4WIRE        ((uint8_t)0x00)
#define LIS3DSH_SERIALINTERFACE_3WIRE        ((uint8_t)0x01)

#define LIS3DSH_INTERRUPT_REQUEST_LATCHED    ((uint8_t)0x00)
#define LIS3DSH_INTERRUPT_REQUEST_PULSED     ((uint8_t)0x20)

#define LIS3DSH_INTERRUPT_1_ENABLE           ((uint8_t)0x88)
#define LIS3DSH_INTERRUPT_2_ENABLE           ((uint8_t)0x00)
#define LIS3DSH_INTERRUPT_1_2_ENABLE         ((uint8_t)0x88)

#define LIS3DSH_INTERRUPT_SIGNAL_LOW         ((uint8_t)0x00)
#define LIS3DSH_INTERRUPT_SIGNAL_HIGH        ((uint8_t)0x40)

#define LIS3DSH_SM_ENABLE                   ((uint8_t)0x01)
#define LIS3DSH_SM_DISABLE                  ((uint8_t)0x00)

#define LIS3DSH_SM_INT1                     ((uint8_t)0x00)
#define LIS3DSH_SM_INT2                     ((uint8_t)0x08)

#define LIS3DSH_BOOT_NORMALMODE              ((uint8_t)0x00)
#define LIS3DSH_BOOT_FORCED                  ((uint8_t)0x80)

#define LIS3DSH_FIFO_BYPASS_MODE             ((uint8_t)0x00)
#define LIS3DSH_FIFO_MODE                    ((uint8_t)0x20)
#define LIS3DSH_FIFO_STREAM_MODE             ((uint8_t)0x40)
#define LIS3DSH_FIFO_SF_TRIGGER_MODE         ((uint8_t)0x60)
#define LIS3DSH_FIFO_BS_TRIGGER_MODE         ((uint8_t)0x80)
#define LIS3DSH_FIFO_BF_TRIGGER_MODE         ((uint8_t)0xE0)

/* Who I am values */
#define LIS3DSH_ID                           0x3F

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE DATA TYPE DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE CONSTANTS
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE VARIABLES
---------------------------------------------------------------------------------------------------------------------*/
/* Array keeping the identifiers of the SPI units used by each accelerometer */
static uint8_t  gSpiUsedByAcc[ACC_NUM] = 
{
    SPI_USED_BY_ACC
};

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC CONSTANTS
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC VARIABLES
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE FUNCTIONS DECLARATIONS
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE FUNCTIONS DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC FUNCTIONS DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/
void Acc_Init(uint8_t idxAcc)
{
    uint16_t data;
    /* Perform a lite range check */
    if(idxAcc >= ACC_NUM)
        return;

    /* Select the accelerometer */    
    Spi_SetCsLow(gSpiUsedByAcc[idxAcc]);
    /* The initialization sequence - LIS3DSH_Sensitivity_2G, LIS3DSH_Filter_800Hz */
    data = (uint16_t) (LIS3DSH_FULLSCALE_2 | (LIS3DSH_FILTER_BW_800 << 8));
    data |= (uint16_t) (LIS3DSH_DATARATE_800 | LIS3DSH_BLOCK_DATA_UPDATE | LIS3DSH_XYZ_ENABLE);
    data |= (uint16_t) (LIS3DSH_SERIALINTERFACE_4WIRE | LIS3DSH_SELFTEST_NORMAL);

    /* The lower byte goes to LIS3DSH_CTRL_REG4, higher byte goes to LIS3DSH_CTRL_REG5. */
    /* Lower byte first - power mode(ODR) and axes enable*/
    Spi_WriteByte(gSpiUsedByAcc[idxAcc],LIS3DSH_CTRL_REG4_ADDR, (uint8_t) data); 
    /* Higher byte now - full scale and self test*/
    Spi_WriteByte(gSpiUsedByAcc[idxAcc],LIS3DSH_CTRL_REG5_ADDR, (uint8_t)(data >> 8));

    /* Unselect the accelerometer */    
    Spi_SetCsHigh(gSpiUsedByAcc[idxAcc]);
}

bool_t Acc_Detect(uint8_t idxAcc)
{
    uint8_t accID;
    
    /* Perform a lite range check */
    if(idxAcc >= ACC_NUM)
        return FALSE;
    
    /* Try to read the ID of the accelerometer. */
    Spi_SetCsLow(idxAcc); Spi_ReadByte(idxAcc, LIS3DSH_WHO_AM_I_ADDR, &accID); Spi_SetCsHigh(idxAcc);
    if(accID == LIS3DSH_ID)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

void Acc_ReadData(uint8_t idxAcc, int16_t* pData) 
{
    uint8_t buffer[6];

    /* Perform a lite range check */
    if(idxAcc >= ACC_NUM)
        return;
    
    /* Read acceleration values */
    Spi_SetCsLow(idxAcc); Spi_ReadByte(idxAcc, LIS3DSH_OUT_X_L_ADDR, buffer);     Spi_SetCsHigh(idxAcc);
    Spi_SetCsLow(idxAcc); Spi_ReadByte(idxAcc, LIS3DSH_OUT_X_H_ADDR, buffer + 1); Spi_SetCsHigh(idxAcc);
    Spi_SetCsLow(idxAcc); Spi_ReadByte(idxAcc, LIS3DSH_OUT_Y_L_ADDR, buffer + 2); Spi_SetCsHigh(idxAcc);
    Spi_SetCsLow(idxAcc); Spi_ReadByte(idxAcc, LIS3DSH_OUT_Y_H_ADDR, buffer + 3); Spi_SetCsHigh(idxAcc);
    Spi_SetCsLow(idxAcc); Spi_ReadByte(idxAcc, LIS3DSH_OUT_Z_L_ADDR, buffer + 4); Spi_SetCsHigh(idxAcc);
    Spi_SetCsLow(idxAcc); Spi_ReadByte(idxAcc, LIS3DSH_OUT_Z_H_ADDR, buffer + 5); Spi_SetCsHigh(idxAcc);
    
    pData[0] = (int16_t)((buffer[1] << 8) + buffer[0]); /* X */
    pData[1] = (int16_t)((buffer[3] << 8) + buffer[2]); /* X */
    pData[2] = (int16_t)((buffer[5] << 8) + buffer[4]); /* X */
}