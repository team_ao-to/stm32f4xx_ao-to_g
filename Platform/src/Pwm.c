/*---------------------------------------------------------------------------------------------------------------------

 (c) Copyright 2016 by Ao-to Corporation. All rights reserved.

 The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets. 
 No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without 
 the specific written permission signed by a legal representative of the Ao-to Corporation.

 Change log:

 ----------     -----------     -------------     ---------------------------------------------------------------------
 Date           Username        Ticket            Change description
 ----------     -----------     -------------     ---------------------------------------------------------------------
 13.11.2016     razvan          AOTO-10           Initial version of the file
---------------------------------------------------------------------------------------------------------------------*/



/*---------------------------------------------------------------------------------------------------------------------
 SECTION: FILES INCLUSION
---------------------------------------------------------------------------------------------------------------------*/
#include "Pwm.h"
#include "Board.h"
#include "stm32f4xx_tim.h"
#include "stm32f4xx_gpio.h"

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE DEFINES
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE DATA TYPE DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/
/* Pointer to function initializing a timer channel */
typedef void (*pfTimerChannelInit_t)(TIM_TypeDef* TIMx, TIM_OCInitTypeDef* TIM_OCInitStruct);
/* Pointer to function loading a configuration on a timer channel */
typedef void (*pfTimerChannelConfig_t)(TIM_TypeDef* TIMx, uint16_t TIM_OCPreload);

typedef struct
{
    TIM_TypeDef*                timer;                          /* Timer                                 */
    pfTimerChannelInit_t        pfTimerChannelInit;             /* Timer channel init function           */
    pfTimerChannelConfig_t      pfTimerChannelConfig;           /* Timer channel config function         */
   __IO uint32_t*               pTimerChannelClkEnableReg;      /* Timer channel clock enable register   */
   uint32_t                     timerChannelClkEnableMask;      /* Timer channel clock enable mask       */
   uint16_t                     timerChannelPrescaler;          /* Timer channel prescaler               */ 
   uint32_t                     timerChannelPeriod;             /* Timer channel period                  */ 
   uint8_t                      timerChannelActivePeriodShift;  /* Timer channel active period shift     */
}pwmTimerChannelConfig_t;

/* Structure containing information about a Pwm pin */
typedef struct
{
    uint16_t        pwmPin;         /* Pin number           */
    GPIO_TypeDef*   pwmPinPort;     /* Pin port             */
    uint32_t        pwmPinPortClk;  /* Pin port clock       */
    uint8_t         pwmPinSource;   /* Pin source           */
    uint8_t         pwmPinAF;       /* Alternate function   */
}pwmPinConfig_t;

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE CONSTANTS
---------------------------------------------------------------------------------------------------------------------*/
/* Structure containing configuration of the timer channels used by Pwm */
static const pwmTimerChannelConfig_t gPwmTimerChannelConfig[PWM_NUM] =
{
    PWM_TIMER_CHANNELS_CONFIG
};

/* Structure containing Pwm pins configuration information */
static const pwmPinConfig_t gPwmPinConfig[PWM_NUM] =
{
    PWM_PINS_CONFIG
};


/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE VARIABLES
---------------------------------------------------------------------------------------------------------------------*/
/* Structure used for pwm channel active period update  */
static TIM_OCInitTypeDef  gTimOCInit;
static uint16_t           gActivePeriodUs[PWM_NUM];

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC CONSTANTS
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC VARIABLES
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE FUNCTIONS DECLARATIONS
---------------------------------------------------------------------------------------------------------------------*/
static void InitPwmGpioPins(void);
static void InitPwmTimerChannels(void);

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE FUNCTIONS DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/
static void InitPwmGpioPins(void)
{
    uint8_t             idx;
    GPIO_InitTypeDef    gpioInitStruct;
    
    /* Init Pwm GPIO pins */
    for(idx = 0; idx < PWM_NUM; idx++)
    {
        /* Enable the Pwm GPIO clock */
        RCC_AHB1PeriphClockCmd(gPwmPinConfig[idx].pwmPinPortClk, ENABLE);

        /* Configure the Pwm GPIO pin */
        gpioInitStruct.GPIO_Pin   = gPwmPinConfig[idx].pwmPin;
        gpioInitStruct.GPIO_Mode  = GPIO_Mode_AF;
        gpioInitStruct.GPIO_OType = GPIO_OType_PP;
        gpioInitStruct.GPIO_PuPd  = GPIO_PuPd_UP;
        gpioInitStruct.GPIO_Speed = GPIO_Speed_100MHz;
        GPIO_Init((GPIO_TypeDef*)gPwmPinConfig[idx].pwmPinPort, &gpioInitStruct);
        /* Connect timer channels to pwm GPIO pins */
        GPIO_PinAFConfig(gPwmPinConfig[idx].pwmPinPort, gPwmPinConfig[idx].pwmPinSource, gPwmPinConfig[idx].pwmPinAF);
    }
}

static void InitPwmTimerChannels(void)
{
    uint8_t                  idx;
    TIM_TimeBaseInitTypeDef  timBaseInit;
    
    /* Init timer channels used by Pwm */
    for(idx = 0; idx < PWM_NUM; idx++)
    {
        /* Enable peripheral clock for the timers */
        *gPwmTimerChannelConfig[idx].pTimerChannelClkEnableReg |= gPwmTimerChannelConfig[idx].timerChannelClkEnableMask;
        /* Configure timer channel initialization struct */
        timBaseInit.TIM_Prescaler       = gPwmTimerChannelConfig[idx].timerChannelPrescaler;
        timBaseInit.TIM_Period          = gPwmTimerChannelConfig[idx].timerChannelPeriod;
        timBaseInit.TIM_ClockDivision   = 0;
        timBaseInit.TIM_CounterMode     = TIM_CounterMode_Up;        
        TIM_TimeBaseInit(gPwmTimerChannelConfig[idx].timer, &timBaseInit);
        
        TIM_ARRPreloadConfig(gPwmTimerChannelConfig[idx].timer, ENABLE);
        /* Enable timers */
        TIM_Cmd(gPwmTimerChannelConfig[idx].timer, ENABLE);
    }
    
    /* Prepare the gTimOCInit structure used for Pwm_SetActivePeriod */
    TIM_OCStructInit(&gTimOCInit);
    gTimOCInit.TIM_OCMode        = TIM_OCMode_PWM1;
    gTimOCInit.TIM_OutputState   = TIM_OutputState_Enable;
    gTimOCInit.TIM_OCPolarity    = TIM_OCPolarity_High;    
}

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC FUNCTIONS DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/
void Pwm_Init(void)
{
    InitPwmGpioPins();
    InitPwmTimerChannels();
}

void Pwm_SetActivePeriod(uint8_t idxPwm, uint16_t activePeriod)
{
    /* For the pwm channels we need to do division by 2 in order to
    translate the usActivePeriod which is received as number of milliseconds */
    gTimOCInit.TIM_Pulse = activePeriod >> gPwmTimerChannelConfig[idxPwm].timerChannelActivePeriodShift;
    /* Init timer channel with the above updated static configuration */
    gPwmTimerChannelConfig[idxPwm].pfTimerChannelInit(gPwmTimerChannelConfig[idxPwm].timer, &gTimOCInit);
    /* Load the timer channel configuration */
    gPwmTimerChannelConfig[idxPwm].pfTimerChannelConfig(gPwmTimerChannelConfig[idxPwm].timer, TIM_OCPreload_Enable);
    /* Store value of active period locally to  be able to retrieve it in Pwm_GetActivePeriod() function */    
    gActivePeriodUs[idxPwm] = activePeriod;
}

uint16_t Pwm_GetActivePeriod(uint8_t idxPwm)
{
    return gActivePeriodUs[idxPwm];
}


