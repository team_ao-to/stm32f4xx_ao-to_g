
---------------------------------------------------------------------------------------------------
TEST_STM32F4xx_AO-TO_G_v0.0.0
Date: 2016.11.08
Reason: First version of the test application used to validate the correct mounting of the hw
components on the 2 boards:G and G+
Notes: -
Changes since previous version: -

---------------------------------------------------------------------------------------------------
REL_STM32F4xx_AO-TO_G_v0.0.0
Date: 2016.10.22
Reason: First version of the project put under source control in Git
Notes: Includes the Nvm component and FreeRTOS OS
Changes since previous version: -
---------------------------------------------------------------------------------------------------
