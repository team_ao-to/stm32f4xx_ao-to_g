/*---------------------------------------------------------------------------------------------------------------------

 (c) Copyright 2016 by Ao-to Corporation. All rights reserved.

 The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
 No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
 the specific written permission signed by a legal representative of the Ao-to Corporation.

 Change log:

 ----------     -----------     -------------     ---------------------------------------------------------------------
 Date           Username        Ticket            Change description
 ----------     -----------     -------------     ---------------------------------------------------------------------
 02.11.2016     razvan          AOTO-8            Various updates to have the hw board validation app in place
 13.11.2016     razvan          AOTO-10           Added PWM platform component
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: FILES INCLUSION
---------------------------------------------------------------------------------------------------------------------*/
/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

/* Application includes. */
#include "Uart.h"
#include "Nvm.h"
#include "Led.h"
#include "Spi.h"
#include "Acc.h"
#include "Pwm.h"

/* Hardware and starter kit includes. */
#include "Board.h"
#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "stm32f4xx_flash.h"

#include "UartPrint.h"

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE DEFINES
---------------------------------------------------------------------------------------------------------------------*/
/* Priority for the single task that this application will start. */
#define mainSTART_TIMER_TASK_PRIORITY    ( tskIDLE_PRIORITY + 1UL )

/* Defines related to NVM testing */
#define NVM_TEST_DATA_MAX_LEN   2048

#define LED_BLINK_INTERVAL      350  /* ms */

#define SHOW_STATUS_INTERVAL    5000  /* ms */

#define ACC_POLL_INTERVAL       10    /* ms */

/* Semaphore used in the UART receive callback to signal to the app that we have received characters in the UART FIFO*/
SemaphoreHandle_t uartSemaphore[UART_NUM] = NULL;
/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE DATA TYPE DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/
/* Declare app-level data structure to hold non-volatile info. */
typedef struct
{
    long start;
    char bulk[NVM_TEST_DATA_MAX_LEN];
    long end;
}nvmTestData_t;

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE CONSTANTS
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE VARIABLES
---------------------------------------------------------------------------------------------------------------------*/
static nvmTestData_t nvmTestData;
/* Variable storing the ID of the currently allocated timer */
static int timerId = 0;

static bool_t gIsAccGPlusAvailable = FALSE;

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC CONSTANTS
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC VARIABLES
---------------------------------------------------------------------------------------------------------------------*/
/* Instantiate and intialize the NVM expected descriptor. Take care that the ID is changed every time that
 * the data structure is changed. */
const nvmDescriptor_t nvmDescriptor = {(void*)&nvmTestData, 0x0006, sizeof(nvmTestData_t)};

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE FUNCTIONS DECLARATIONS
---------------------------------------------------------------------------------------------------------------------*/
/* Set up the hardware ready to run this demo. */
static void SetupHardware(void);

static void testUarts(uint8_t numMessages);
static void testNvm(void);

static void Broadcast_String(uint8_t* pString);

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PRIVATE FUNCTIONS DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/

/* UART RX callbacks. This gets executed from the IRQ handler - should just set an event for the interested threads and get out.
   Here we are in an ISR context..."FromISR" variant of the xSemaphoreGive should be called.
*/
static void appUartCallback(uint8_t idxUart)
{
    if(xSemaphoreGiveFromISR(uartSemaphore[idxUart], NULL) != pdTRUE)
    {
        //Some error counter should get updated.
    }
}

static void SetupHardware(void)
{
    uint8_t idx;

    /* Ensure all priority bits are assigned as preemption priority bits. This needs to be done before OS init:
    http://www.freertos.org/FreeRTOS_Support_Forum_Archive/August_2013/freertos_STM32_IAR_amp_port.c_configASSERT_8618788.html
    */
    NVIC_PriorityGroupConfig( NVIC_PriorityGroup_4 );

    /* Setup STM32 system (clock, PLL and Flash configuration) */
    SystemInit();
    /* Initialize the leds. */
    Led_Init();
    /* Initialize Nvm component */
    Nvm_Init();

    /* Initialze the SPI interfaces before initializing the Accelerometer components. */
    for(idx = 0; idx < SPI_NUM; idx++)
    {
        Spi_Init(idx);
    }

    /* Initialze the accelerometer on the G board. The accelerometer on the G+ board
    will be initialized only if it will be detected */
    Acc_Init(Acc_G_c);

    /* Init the UARTs */
    for(uint8_t idxUart = 0; idxUart < UART_NUM; idxUart++)
    {
        Uart_Init(idxUart, 115200, 5, appUartCallback);
        /* Allocate the UART receive semaphore as a counting semaphore, max events held is 5, initial count of 0 */
        uartSemaphore[idxUart] = xSemaphoreCreateCounting( 5, 0 );
    }

    /* Init the Pwm module */
    Pwm_Init();
}

static void testUarts(uint8_t numMessages)
{
    uint8_t uartTxBuffer[10] = {'U', 'A', 'R', 'T', 'M', 'S', 'G', '0', '\n', '\r'};
    uint8_t idxUart, idxMsg;

    for(idxUart = 0; idxUart < UART_NUM; idxUart++)
    {
        for(idxMsg = 0; idxMsg < numMessages; idxMsg++)
        {
            Uart_Send(idxUart, uartTxBuffer, 10);
            uartTxBuffer[7]++;
            if(uartTxBuffer[7] > '9') uartTxBuffer[7] = '0';
        }
    }
}

static void testNvm(void)
{
    int i, j;

    nvmTestData.start = 0xAAAAAAAA;
    nvmTestData.end = 0xBBBBBBBB;

    /* This will trigger one sector change. */
    for (j = 1; j < 13; j++)
    {
        for(i = 0; i < NVM_TEST_DATA_MAX_LEN; i++)
        {
            nvmTestData.bulk[i] = j;
        }
        Nvm_Save();
    }

    for(i = 0; i < NVM_TEST_DATA_MAX_LEN; i++)
    {
        nvmTestData.bulk[i] = 15;
    }

    Nvm_Restore();

    /* This will trigger a second sector change. */
    for (j = 14; j < 20; j++)
    {
        for(i = 0; i < NVM_TEST_DATA_MAX_LEN; i++)
        {
            nvmTestData.bulk[i] = j;
        }
        Nvm_Save();
    }

    for(i = 0; i < NVM_TEST_DATA_MAX_LEN; i++)
    {
        nvmTestData.bulk[i] = 0xFF;
    }

    Nvm_Restore();
}

static void LedTimerCallback( TimerHandle_t pxTimer )
{
    static bool_t isLedOn = FALSE;

    /* Always blink the status LED, showing that we're alive */
    Led_On(Led_Status_c, isLedOn);

    /* Use this LED timer to check if the accelerometer on the G+ board is available */
    if(Acc_Detect(Acc_G_Plus_c) == TRUE)
    {
        /* If the G+ Acc was just connected, we need to initialize it first */
        if(gIsAccGPlusAvailable == FALSE)
        {
            Acc_Init(Acc_G_Plus_c);
            gIsAccGPlusAvailable = TRUE;
        }
        else
        {
            /* We're here because the G+ acc is already available for some while. Just blink the G+ LED */
            Led_On(Led_G_Plus_c, isLedOn);
        }
    }
    else
    {
        Led_On(Led_G_Plus_c, FALSE);
        gIsAccGPlusAvailable = FALSE;
    }

    isLedOn = !isLedOn;
}

static void AccTimerCallback( TimerHandle_t pxTimer )
{
    uint8_t idx;
    int16_t rawAccData[ACC_NUM][ACC_NUM_AXES];

    /* Read data from on the board accelerometer */
    Acc_ReadData(Acc_G_c, &rawAccData[Acc_G_c][0]);

    /* Read data from G+ accelerometer, if it is connected and initialized */
    if(gIsAccGPlusAvailable)
    {
        Acc_ReadData(Acc_G_Plus_c, &rawAccData[Acc_G_Plus_c][0]);
    }
    else
    {
        for(idx = 0; idx < ACC_NUM_AXES; idx++)
        {
            rawAccData[Acc_G_Plus_c][idx] = 0;
        }
    }
}

/* Application task */
static portTASK_FUNCTION(AppTask, pvParameters )
{
    TimerHandle_t timerHandle;

    /* Create one periodic (auto-reload) timer for driving the LEDs */
    timerHandle = xTimerCreate("LedTimer", LED_BLINK_INTERVAL/portTICK_PERIOD_MS, pdTRUE, ( void * )timerId, LedTimerCallback);
    timerId++;
    /* If the timer was created, start it */
    if( timerHandle != NULL )
    {
        (void)xTimerStart(timerHandle, 0);
    }

    /* Create one periodic (auto-reload) timer for reading data from accelerometers */
    timerHandle = xTimerCreate("AccTimer", ACC_POLL_INTERVAL/portTICK_PERIOD_MS, pdTRUE, ( void * )timerId, AccTimerCallback);
    timerId++;
    /* If the timer was created, start it */
    if( timerHandle != NULL )
    {
        (void)xTimerStart(timerHandle, 0);
    }

    /* Display status message on both Uarts */
    Broadcast_String("Application successfully started.\n\r");

    Pwm_SetActivePeriod(PWM0, 1000);
    Pwm_SetActivePeriod(PWM1, 1200);
    Pwm_SetActivePeriod(PWM2, 1400);
    Pwm_SetActivePeriod(PWM3, 1600);
    Pwm_SetActivePeriod(PWM4, 1800);

    /* Never exit, continuously handle events/messages for the main app thread */
    while(1)
    {
        /*Check the UART receive semaphores, read and display the characters if some were read.*/
        for(uint8_t idxUart = 0; idxUart < UART_NUM; idxUart++)
        {
            uint8_t readBuffer[64];
            uint16_t read = 0;
            /* See if the semaphore was signalled. Do not wait.*/
            if(xSemaphoreTake(uartSemaphore[idxUart], (TickType_t)0) == pdTRUE)
            {
                Uart_Read(idxUart, readBuffer, 64, &read);
            }
            /* Write the chars back */
            if(read != 0)
            {
                Uart_Send(idxUart, readBuffer, read);
            }
        }
    }
}

void Broadcast_String(uint8_t* pString)
{
    UartPrint_String(Uart_SerialComm_c, pString);
    UartPrint_String(Uart_Bluetooth_c,  pString);
}

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC FUNCTIONS DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/

int main(void)
{
    /* Configure the hardware ready to run the test. */
    SetupHardware();

    /* Test the Uarts*/
    testUarts(13);
    /* Test the Nvm component */
    // testNvm();

    /* Spawn the task. */
    xTaskCreate( AppTask, "App Task", configMINIMAL_STACK_SIZE, NULL, mainSTART_TIMER_TASK_PRIORITY, ( TaskHandle_t * ) NULL );

    /* Start the scheduler. */
    vTaskStartScheduler();

    /* If all is well, the scheduler will now be running, and the following line
    will never be reached.  If the following line does execute, then there was
    insufficient FreeRTOS heap memory available for the idle and/or timer tasks
    to be created.  See the memory management section on the FreeRTOS web site
    for more details. */
    for( ;; );
}
