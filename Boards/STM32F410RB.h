/*---------------------------------------------------------------------------------------------------------------------

 (c) Copyright 2016 by Ao-to Corporation. All rights reserved.

 The material in this file is the property of Ao-to Corporation, contains confidential information and trade secrets.
 No part of this document may be disclosed, reproduced, copied, transmitted, printed or used for any purpose without
 the specific written permission signed by a legal representative of the Ao-to Corporation.

 Change log:

 ----------     -----------     -------------     ---------------------------------------------------------------------
 Date           Username        Ticket            Change description
 ----------     -----------     -------------     ---------------------------------------------------------------------
 22.10.2016     razvan          AOTO-1            Add Led component in the platform folder
 29.10.2016     nicu            AOTO-5            Add Acc component in the platform folder
 01.11.2016     nicu            AOTO-6            Added support for multiple UART instances
 01.11.2016     nicu            AOTO-7            Added support for board dependant clock configurations
 02.11.2016     razvan          AOTO-8            Various updates to have the hw board validation app in place
 12.11.2016     razvan          AOTO-11           Update LED info in boards header files following ACC, SPI, UART format
 13.11.2016     razvan          AOTO-10           Added PWM platform component
--------------------------------------------------------------------------------------------------------------------*/

#ifndef _STM32F410RB_H
#define _STM32F410RB_H

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: FILES INCLUSION
---------------------------------------------------------------------------------------------------------------------*/
#include "Platform_Types.h"

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC DEFINES
---------------------------------------------------------------------------------------------------------------------*/
/* Defines related to board clocking options: HSI/HSE, target frequency, resulting values (PLL, flash access latency)
 * These values are used in /proj/STM32F4xx/startup/system_stm32f4xx.c and /proj/STM32F4xx/inc/stm32f4xx.h
 *
 * !!!
 * NOTE THAT ONLY THE HSE CLOCKING MODE IS SUPPORTED. To boot using HSI, a suitable system_stm32f4xx.c would have
 * to be generated using the STM32F4xx_Clock_Configuration_V1.0.1.xls from ST.
 */
#define BOARD_HSE_VALUE         8000000
#define BOARD_HSI_VALUE         16000000
#define BOARD_SYSCLK            100000000   /* In Hz*/
#define BOARD_PLL_M             4
#define BOARD_PLL_N             200
#define BOARD_PLL_P             4
#define BOARD_PLL_Q             9
#define BOARD_FLASH_ACR_LATENCY FLASH_ACR_LATENCY_3WS


/* Defines related to LEDs on the board */
#define LED_NUM     3

#define LED_PINS_CONFIG                                         \
    {                                                           \
        /* Led_Status_c */                                      \
        GPIO_Pin_4,             /* Pin number           */      \
        GPIOC,                  /* Pin port             */      \
        RCC_AHB1Periph_GPIOC,   /* Pin port clock       */      \
    },                                                          \
    {                                                           \
        /* Led_G_Plus_c */                                      \
        GPIO_Pin_15,            /* Pin number           */      \
        GPIOC,                  /* Pin port             */      \
        RCC_AHB1Periph_GPIOC,   /* Pin port clock       */      \
    },                                                          \
    {                                                           \
        /* Led_Uart_c */                                        \
        GPIO_Pin_12,            /* Pin number           */      \
        GPIOB,                  /* Pin port             */      \
        RCC_AHB1Periph_GPIOB,   /* Pin port clock       */      \
    }

/* Defines related to UARTs on the board */
#define UART_NUM    2

#define USART_INFO                                              \
    {                                                           \
        USART1,                 /* USART Unit       */          \
        USART1_IRQn,            /* USART IRQ no     */          \
        &(RCC->APB2ENR),        /* USART Clk Enable Register */ \
        RCC_APB2ENR_USART1EN    /* USART Clock Enable Mask  */  \
    },                                                          \
    {                                                           \
        USART6,                 /* USART Unit       */          \
        USART6_IRQn,            /* USART IRQ no     */          \
        &(RCC->APB2ENR),        /* USART Clk Enable Register */ \
        RCC_APB2ENR_USART6EN    /* USART Clock Enable Mask  */  \
    }

#define UART_PORT_INFO                                          \
    {                                                           \
        GPIOA,                  /* GPIO Port        */          \
        RCC_AHB1Periph_GPIOA,   /* GPIO Port clock  */          \
        GPIO_Pin_9,             /* TX pin           */          \
        GPIO_Pin_10,            /* RX pin           */          \
        GPIO_PinSource9,        /* TX pin source    */          \
        GPIO_PinSource10,       /* RX pin source    */          \
        GPIO_AF_USART1          /* Alternate func   */          \
    },                                                          \
    {                                                           \
        GPIOA,                  /* GPIO Port        */          \
        RCC_AHB1Periph_GPIOA,   /* GPIO Port clock  */          \
        GPIO_Pin_11,            /* TX pin           */          \
        GPIO_Pin_12,            /* RX pin           */          \
        GPIO_PinSource11,       /* TX pin source    */          \
        GPIO_PinSource12,       /* RX pin source    */          \
        GPIO_AF_USART6          /* Alternate func   */          \
    }


/* Defines related to SPI */
#define SPI_NUM         2

#define SPI_CONFIG                                              \
    {                                                           \
        SPI1,                /* SPI Unit                  */    \
        &(RCC->APB2ENR),     /* SPI Clock Enable Register */    \
        RCC_APB2ENR_SPI1EN   /* SPI Clock Enable Mask     */    \
    },                                                          \
    {                                                           \
        SPI2,                /* SPI Unit                  */    \
        &(RCC->APB1ENR),     /* SPI Clock Enable Register */    \
        RCC_APB1ENR_SPI2EN   /* SPI Clock Enable Mask     */    \
    }

#define SPI_PINS_CONFIG                                         \
    {                                                           \
        /* SPI1 pins */                                         \
        {                                                       \
            /* MISO */                                          \
            GPIO_Pin_6,             /* Pin number           */  \
            GPIOA,                  /* Pin port             */  \
            RCC_AHB1Periph_GPIOA,   /* Pin port clock       */  \
            GPIO_PinSource6,        /* Pin source           */  \
            GPIO_AF_SPI1            /* Alternate function   */  \
        },                                                      \
        {                                                       \
            /* MOSI */                                          \
            GPIO_Pin_7,             /* Pin number           */  \
            GPIOA,                  /* Pin port             */  \
            RCC_AHB1Periph_GPIOA,   /* Pin port clock       */  \
            GPIO_PinSource7,        /* Pin source           */  \
            GPIO_AF_SPI1            /* Alternate function   */  \
        },                                                      \
        {                                                       \
            /* SCK */                                           \
            GPIO_Pin_5,             /* Pin number           */  \
            GPIOA,                  /* Pin port             */  \
            RCC_AHB1Periph_GPIOA,   /* Pin port clock       */  \
            GPIO_PinSource5,        /* Pin source           */  \
            GPIO_AF_SPI1            /* Alternate function   */  \
        },                                                      \
        {                                                       \
            /* CS */                                            \
            GPIO_Pin_4,             /* Pin number           */  \
            GPIOA,                  /* Pin port             */  \
            RCC_AHB1Periph_GPIOA,   /* Pin port clock       */  \
            0x00,                   /* Pin source           */  \
            0x00                    /* Alternate function   */  \
        }                                                       \
    },                                                          \
    {                                                           \
        /* SPI2 pins */                                         \
        {                                                       \
            /* MISO */                                          \
            GPIO_Pin_2,             /* Pin number           */  \
            GPIOC,                  /* Pin port             */  \
            RCC_AHB1Periph_GPIOC,   /* Pin port clock       */  \
            GPIO_PinSource2,        /* Pin source           */  \
            GPIO_AF_SPI2            /* Alternate function   */  \
        },                                                      \
        {                                                       \
            /* MOSI */                                          \
            GPIO_Pin_3,             /* Pin number           */  \
            GPIOC,                  /* Pin port             */  \
            RCC_AHB1Periph_GPIOC,   /* Pin port clock       */  \
            GPIO_PinSource3,        /* Pin source           */  \
            GPIO_AF_SPI2            /* Alternate function   */  \
        },                                                      \
        {                                                       \
            /* SCK  */                                          \
            GPIO_Pin_10,             /* Pin number           */  \
            GPIOB,                  /* Pin port             */  \
            RCC_AHB1Periph_GPIOB,   /* Pin port clock       */  \
            GPIO_PinSource10,        /* Pin source           */  \
            GPIO_AF_SPI2            /* Alternate function   */  \
        },                                                      \
        {                                                       \
            /* CS   */                                          \
            GPIO_Pin_9,             /* Pin number           */  \
            GPIOB,                  /* Pin port             */  \
            RCC_AHB1Periph_GPIOB,   /* Pin port clock       */  \
            0x00,                   /* Pin source           */  \
            0x00                    /* Alternate function   */  \
        }                                                       \
    }

    
/* Defines related to Accelerometers on the board */
#define ACC_NUM             2

/* Defining the SPI units used by each acc on the board */    
#define SPI_USED_BY_ACC                                         \
        SPI_1, /* Used by Acc_G_c      */                       \
        SPI_2  /* Used by Acc_G_Plus_c */


/* Defines related to Pwm channels on the board */
/*
PWM1 - PB11 - TIM5_CH4
PWM2 - PA0  - TIM5_CH1  
PWM3 - PA1  - TIM5_CH2
PWM4 - PA2  - TIM9_CH1
PWM5 - PA3  - TIM9_CH2
*/

#define PWM_NUM             5

#define PWM_PINS_CONFIG                                         \
    {                                                           \
        GPIO_Pin_11,            /* Pin number           */      \
        GPIOB,                  /* Pin port             */      \
        RCC_AHB1Periph_GPIOB,   /* Pin port clock       */      \
        GPIO_PinSource11,       /* Pin source           */      \
        GPIO_AF_TIM5            /* Alternate function   */      \
    },                                                          \
    {                                                           \
        GPIO_Pin_0,             /* Pin number           */      \
        GPIOA,                  /* Pin port             */      \
        RCC_AHB1Periph_GPIOA,   /* Pin port clock       */      \
        GPIO_PinSource0,        /* Pin source           */      \
        GPIO_AF_TIM5            /* Alternate function   */      \
    },                                                          \
    {                                                           \
        GPIO_Pin_1,             /* Pin number           */      \
        GPIOA,                  /* Pin port             */      \
        RCC_AHB1Periph_GPIOA,   /* Pin port clock       */      \
        GPIO_PinSource1,        /* Pin source           */      \
        GPIO_AF_TIM5            /* Alternate function   */      \
    },                                                          \
    {                                                           \
        GPIO_Pin_2,             /* Pin number           */      \
        GPIOA,                  /* Pin port             */      \
        RCC_AHB1Periph_GPIOA,   /* Pin port clock       */      \
        GPIO_PinSource2,        /* Pin source           */      \
        GPIO_AF_TIM9            /* Alternate function   */      \
    },                                                          \
    {                                                           \
        GPIO_Pin_3,             /* Pin number           */      \
        GPIOA,                  /* Pin port             */      \
        RCC_AHB1Periph_GPIOA,   /* Pin port clock       */      \
        GPIO_PinSource3,        /* Pin source           */      \
        GPIO_AF_TIM9            /* Alternate function   */      \
    }                                                           \

#define PWM_TIMER_CHANNELS_CONFIG                                           \
    {                                                                       \
        TIM5,                   /* Timer                                 */ \
        TIM_OC4Init,            /* Timer channel init function           */ \
        TIM_OC4PreloadConfig,   /* Timer channel config function         */ \
        &(RCC->APB1ENR),        /* Timer channel clock enable register   */ \
        RCC_APB1ENR_TIM5EN,     /* Timer channel clock enable mask       */ \
        100 - 1,                /* Timer channel prescaler               */ \
        5000 - 1,               /* Timer channel period                  */ \
        1                       /* Timer channel active period shift     */ \
    },                                                                      \
    {                                                                       \
        TIM5,                   /* Timer                                 */ \
        TIM_OC1Init,            /* Timer channel init function           */ \
        TIM_OC1PreloadConfig,   /* Timer channel config function         */ \
        &(RCC->APB1ENR),        /* Timer channel clock enable register   */ \
        RCC_APB1ENR_TIM5EN,     /* Timer channel clock enable mask       */ \
        100 - 1,                /* Timer channel prescaler               */ \
        5000 - 1,               /* Timer channel period                  */ \
        1                       /* Timer channel active period shift     */ \
    },                                                                      \
    {                                                                       \
        TIM5,                   /* Timer                                 */ \
        TIM_OC2Init,            /* Timer channel init function           */ \
        TIM_OC2PreloadConfig,   /* Timer channel config function         */ \
        &(RCC->APB1ENR),        /* Timer channel clock enable register   */ \
        RCC_APB1ENR_TIM5EN,     /* Timer channel clock enable mask       */ \
        100 - 1,                /* Timer channel prescaler               */ \
        5000 - 1,               /* Timer channel period                  */ \
        1                       /* Timer channel active period shift     */ \
    },                                                                      \
    {                                                                       \
        TIM9,                   /* Timer                                 */ \
        TIM_OC1Init,            /* Timer channel init function           */ \
        TIM_OC1PreloadConfig,   /* Timer channel config function         */ \
        &(RCC->APB2ENR),        /* Timer channel clock enable register   */ \
        RCC_APB2ENR_TIM9EN,     /* Timer channel clock enable mask       */ \
        100 - 1,                /* Timer channel prescaler               */ \
        10000 - 1,              /* Timer channel period                  */ \
        0                       /* Timer channel active period shift     */ \
    },                                                                      \
    {                                                                       \
        TIM9,                   /* Timer                                 */ \
        TIM_OC2Init,            /* Timer channel init function           */ \
        TIM_OC2PreloadConfig,   /* Timer channel config function         */ \
        &(RCC->APB2ENR),        /* Timer channel clock enable register   */ \
        RCC_APB2ENR_TIM9EN,     /* Timer channel clock enable mask       */ \
        100 - 1,                /* Timer channel prescaler               */ \
        10000-1,                /* Timer channel period                  */ \
        0                       /* Timer channel active period shift     */ \
    }

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC DATA TYPE DEFINITIONS
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC CONSTANTS
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC VARIABLES
---------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------------------
 SECTION: PUBLIC FUNCTIONS DECLARATIONS
---------------------------------------------------------------------------------------------------------------------*/


#endif /* _STM32F410RB_H */
